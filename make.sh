#!/bin/bash
if [ -d Servers ] || [-f Servers]
then
sudo rm -R Servers/ 
fi
mkdir Servers 
echo '#!/bin/bash' >./Servers/docker-compose_up.sh
echo '#!/bin/bash' >./Servers/docker_run.sh
echo '#!/bin/bash' >./Servers/kaniko-build.sh
echo '#!/bin/bash' >./Servers/docker_build.sh
cat << EOF >> ./Servers/docker-compose.yml
version: '3'
services:
EOF
for ((i=1;i<=2;i++))
  do
  let p=8080+$i
  mkdir Servers/$i  Servers/$i/conf Servers/$i/www
cat << EOF > ./Servers/$i/www/index.html
<html><head>
</head>
<body>
Hello World from Server #$i!!!
</body>
</html>
EOF
cat << EOF > ./Servers/$i/conf/default.conf
server {
    listen  $p; 
    index index.html;
    server_name server$i.test;
    root /var/www;
}
EOF
cat << EOF > ./Servers/$i/Dockerfile
FROM nginx:latest
ADD conf/default.conf /etc/nginx/conf.d
RUN rm -rf /var/www && mkdir /var/www
ADD www/index.html /var/www
EXPOSE $p
EOF
cat << EOF >> ./Servers/docker-compose.yml
    nginx$i:
        image: nginx:latest
        ports:
            - "$p:$p"
        volumes:
            - $PWD/Servers/$i/conf:/etc/nginx/conf.d
            - $PWD/Servers/$i/www:/var/www
EOF
cat << EOF >> ./Servers/kaniko-build.sh
docker run -ti --rm \
  -v $PWD/Servers/$i:/workspace \
  -v $PWD/config.json:/kaniko/.docker/config.json:ro \
  gcr.io/kaniko-project/executor:debug \
  --cache \
  --dockerfile=Dockerfile \
  --context=/workspace \
  --destination=registry.gitlab.com/konkrov/1test:latest
EOF
echo "docker build -t nginx_$i $PWD/Servers/$i" >> ./Servers/docker_build.sh

echo "docker container run --name nginx_$i -p $p:$p --rm nginx_$i &" >> ./Servers/docker_run.sh

done

echo 'docker-compose up' >> ./Servers/docker-compose_up.sh

chmod 775 ./Servers/docker_run.sh
chmod 775 ./Servers/docker_build.sh
chmod 775 ./Servers/kaniko-build.sh
chmod 775 ./Servers/docker-compose_up.sh 
